use failure::Error;
use ndarray::prelude::*;
//use ndarray_linalg::Inverse;
use byteorder::{BigEndian, ReadBytesExt, WriteBytesExt};
use rand::prelude::*;
use rayon::prelude::*;
use std::{fs::File, time::Instant};

const W: usize = 5;
const H: usize = 5;
const ITER_STEP: u128 = 500000;

#[derive(Clone)]
struct Grid {
    m: Array2<f64>,
    buffer: Vec<Array1<f64>>,
    pos: usize,
    top: [bool; W],
    bottom: [bool; W],
    seed: bool,
    steps: usize,
}

enum StepResult {
    Finished(usize),
    Continue,
}

impl Default for Grid {
    fn default() -> Self {
        let mut m = Array::zeros((W * H, W * H));
        for i in 0..(H - 1) * W {
            m[[i, i + W]] = 1.;
        }
        for i in 0..H {
            for j in 0..W - 1 {
                m[[j + i * W, j + 1 + i * W]] = 1.;
            }
        }
        m = m.clone() + m.clone().reversed_axes();
        for c in 0..W * H {
            let s = m.column_mut(c);
            let n = s.sum();
            s.mapv_into(|x| x / n);
        }

        let mut buffer = Vec::with_capacity(W * H);
        for i in 0..(W * H) {
            let mut v = Array::zeros(W * H);
            v[[i]] = 1.;
            buffer.push(m.dot(&v).clone());
        }

        Grid {
            m,
            pos: (W * H - 1) / 2,
            buffer,
            bottom: [true; W],
            top: [false; W],
            seed: false,
            steps: 0,
        }
    }
}

impl Grid {
    fn step(&mut self) -> StepResult {
        let v = &self.buffer[self.pos];
        let mut sum = 0.;
        let mut ind = 0;
        let mut rng = rand::thread_rng();
        let r: f64 = rng.gen();
        for x in v.iter() {
            sum += x;
            if r <= sum {
                break;
            }
            ind += 1;
        }
        if self.seed {
            if ind < W {
                if !self.top[ind] {
                    self.top[ind] = true;
                    self.seed = false;
                }
            }
        } else {
            if ind >= W * (H - 1) {
                let i = ind - W * (H - 1);
                if self.bottom[i] {
                    self.bottom[i] = false;
                    self.seed = true;
                }
            }
        }
        self.steps += 1;
        self.pos = ind;
        if self.top.iter().all(|&x| x) {
            StepResult::Finished(self.steps)
        } else {
            StepResult::Continue
        }
    }
}

fn sample(mut g: Grid) -> u128 {
    loop {
        if let StepResult::Finished(steps) = g.step() {
            return steps as u128;
        }
    }
}

fn main() -> Result<(), Error> {
    let mut counter: u128 = 0;
    let mut sum: u128 = 0;
    let g = Grid::default();
    let mut last = 0.;
    let mut start = Instant::now();
    if let Ok(mut file) = File::open("status") {
        sum = file.read_u128::<BigEndian>()?;
        counter = file.read_u128::<BigEndian>()?;
        last = (sum as f64) / (counter as f64);
    };
    loop {
        let g = g.clone();
        sum += (0..ITER_STEP)
            .into_par_iter()
            .map(move |_| sample(g.clone()))
            .sum::<u128>();
        counter += ITER_STEP;
        let res = (sum as f64) / (counter as f64);
        println!(
            "{:15} / {:7.2}M = {:.6}     Δ = {:12.8}     {:7.2}k iter/s",
            sum,
            (counter as f64) / 1e6,
            res,
            (res - last).abs(),
            (ITER_STEP as f64 * 1e6) / (start.elapsed().as_nanos() as f64)
        );
        last = res;
        let mut file = File::create("status")?;
        file.write_u128::<BigEndian>(sum)?;
        file.write_u128::<BigEndian>(counter)?;
        start = Instant::now();
    }
}
